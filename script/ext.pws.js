// So we have support for jquery and medawiki codebase
// More Infos can be found here
// https://www.mediawiki.org/wiki/Manual:Interface/JavaScript
( function ( mw, $ ) {

  mw.loader.load( 'jquery.ui.autocomplete' );

  var urlPrefix      = mw.config.values.wgServer + mw.config.values.wgScriptPath
  var localUserId    = mw.config.values.wgUserId;
  var secretKeys     = []; 
  var secrets        = $('div.secret');
  var groups         = [];


  for (var i = 0; i < secrets.length; i++){
    aKey = $(secrets[i]).data('secret');
    var aSecretObject = {
      secretKey      : aKey,
      secretItSelf   : "???",
      secretOwnerId  : -1,
      secretExists   : false, 
      secretEditable : false,
      secretViewable : false,
      secretElement  : secrets[i]
    };
    secretKeys[aKey] = aSecretObject;
    checkSecretExists(aSecretObject);
  }


  if ($('div.pws_group_list').length > 0){    
    getGroups();
  }


  if ($('div.pws_group_creating').length > 0){        
    if (localUserId > 0) { 
      anGroupLabel   = '<span>Please create new Group.</span><br>';
      anGroupInput   = '<input  class="pws_group_create" type="text" placeholder="Type group name here."></input>';
      anSubmitButton = '<button class="pws_group_create">Create</button>';    
      $('div.pws_group_creating').html(anGroupLabel + anGroupInput + anSubmitButton);
      var aGroupCreateObject          = {};
      aGroupCreateObject.anGroupInput = $('input.pws_group_create');
      aGroupCreateObject.anGroupDiv   = $('div.pws_group_creating');
      $('button.pws_group_create').click( $.proxy(onGroupCreateClick, aGroupCreateObject));
    } else {
      anGroupLabel   = '<p class="pws-warning">You have to be logged in to create a group.</p><br>';
      $('div.pws_group_creating').html(anGroupLabel); 
    }  
  }


  function onGroupCreateClick(anUIEvent){ 
    aNewGroupName = this.anGroupInput.val();
    if ((aNewGroupName != null ) && (aNewGroupName.length >= 2)){
      $.ajax({
        url     : urlPrefix + "/api.php?action=PassWordSafe&method=createGroup&groupName=" + aNewGroupName + "&format=json",
        context : this
      }).done(function(data){      
        // We Update all the Known Groups
        this.anGroupInput.val('');
        getGroups();
      });
    } else {
      console.log('New group Name has to be longer then 2 Chars');
    }  
  }


  function renderSecret(aSecretObject){
    if ((mw.config.values.wgIsProbablyEditable == true) && (mw.config.values.wgUserName != null)) {
      if (aSecretObject.secretExists){
        if (aSecretObject.secretViewable){
          getSecret(aSecretObject);          
        } else {
          showError(aSecretObject, 'You have no right to see this secret!');
        }
      } else {
        createSecretForTheFirstTime(aSecretObject);
      }    
    } else {
      console.log('|-> No Edit');
      showError(aSecretObject,'You have to log in to create or view a secret.');
    }    
  }


  // This Function appends Icons to Edit Secrets
  // * Edit Secret
  // * Add Users to See
  // * Add Group to See
  function showSecret(aSecretObject){
    var aSecretIcon        = '<a class="pws-lock" href="#" title="SecretKey = ' + aSecretObject.secretKey + '"> </a>'
    var aSecretKey         = '<strong>' + aSecretObject.secretKey + ': </strong>';
    var aSecretSpan        = '<span class="the_secret">' + aSecretObject.secretItSelf + '</span>';
    var anEditRightIcon    = '<a href="#" class="pws-info edit_secret_rights" title="Click to Show Infos or Edit Permissions."/>';
    $(aSecretObject.secretElement).html(aSecretIcon + aSecretSpan + anEditRightIcon);
    $(aSecretObject.secretElement).find('a.edit_secret_rights').click($.proxy(onEditSecretRightsClick, aSecretObject)); 
  }





  function showGroupInformations(anUIEvent){
    anUIEvent.preventDefault();
    var aGroup = groups[$(anUIEvent.target).data('group_id')];   
    if ( aGroup != null){
      var aModal   = '<div class="pws_modal pws_modal_dialog" >'
                   + '  <div>'
                   + '    <a href="#close2" title="Close" class="pws_modal_close">X</a>'
                   + '    <div style="max-height:500px;overflow:auto;">'
                   + '      <h3>Informations about the group: <strong>' + aGroup.name + '</strong></h3>'
                   + '      <h4>Group Editors</h4>';
      if (aGroup.editable){
        aModal    += '      <input type="text" class="pws_select_group_editor pws_input" placeholder="Search for users to give group edit rights."></input>';
      } else {
        aModal    += '      <p class="pws-warning">You have no permission to edit this group!</p>';
      }      
      aModal      += '      <ul class="allowedGroupEditors plainList">'
                   + '        <li>No Group Editors present.</li>'
                   + '      </ul>'
                   + '      <h4>Group Members</h4>';
      if (aGroup.editable){
        aModal    += '      <input type="text" class="pws_select_group_member pws_input" placeholder="Search for members to add to this group."></input>';
      } else {
        aModal    += '      <p class="pws-warning">You have no permission to add members to this group!</p>';
      }      
      aModal      += '      <ul class="groupMembers plainList">'
                   + '        <li>No Group Members present.</li>'
                   + '      </ul>'                         
                   + '    </div>'
                   + '  </div>'  
                   + '</div>'; 
      $('body').append(aModal);   
      $('.pws_modal_close').click(function(){
        $('div.pws_modal').remove();
      });          
      insertAllowedGroupEditors(aGroup);
      insertGroupMembers(aGroup);
      buildGroupEditingAutoCompleteStuff(aGroup);
    }    
  }



  function insertAllowedGroupEditors(aGroupObject){    
    $.ajax({
      url : urlPrefix + "/api.php?action=PassWordSafe&method=getGroupEditors&groupId=" + aGroupObject.id + "&format=json",
    }).done( function(data){
      if ((data != null) && (data.PassWordSafe != null)){
        var groupEditors    = data.PassWordSafe.groupEditors;
        var groupEditorTags = '';
        for (var a = 0; a < groupEditors.length; a++ ){
          groupEditorTags   +=  '<li>'
                            +      '<a href="#" class="pws_user">'
                            +      '<a href="' + urlPrefix + '/index.php?title=User:' + groupEditors[a].user_name + '" >' +  groupEditors[a].user_name + '</a>';
          if ((aGroupObject.editable == true) && (aGroupObject.owner != groupEditors[a].user_id)) {
            groupEditorTags +=     '<a href="#" class="pws_trash remove_group_editor" data-editor_id="' + groupEditors[a].user_id + '" title="Remove this Editor of Group." />';
          }                 
          groupEditorTags   +=  '</li>';
        }
        $('ul.allowedGroupEditors').html(groupEditorTags);
        $('a.remove_group_editor').click( $.proxy(onRemoveGroupEditorClick, aGroupObject ));            
      };  
    });
  }


  function insertGroupMembers(aGroupObject){    
    $.ajax({
      url : urlPrefix + "/api.php?action=PassWordSafe&method=getGroupMembers&groupId=" + aGroupObject.id + "&format=json",
    }).done( function(data){
      if ((data != null) && (data.PassWordSafe != null)){
        var groupMembers    = data.PassWordSafe.groupMembers;
        var groupMemberTags = '';
        for (var a = 0; a < groupMembers.length; a++ ){
          groupMemberTags   +=  '<li>'
                            +      '<a href="#" class="pws_user">'
                            +      '<a href="' + urlPrefix + '/index.php?title=User:' + groupMembers[a].user_name + '">' +  groupMembers[a].user_name  + '</a>';
          // you can't remove the owner of a group                             
          if ((aGroupObject.editable == true) && (aGroupObject.owner != groupMembers[a].user_id)) {
            groupMemberTags +=     '<a href="#" class="pws_trash remove_group_member" data-member_id="' + groupMembers[a].user_id + '" title="Remove this Member of Group." />'
          }                 
          groupMemberTags   +=  '</li>';
        }
        $('ul.groupMembers').html(groupMemberTags);    
        $('a.remove_group_member').click( $.proxy(onRemoveGroupMemberClick, aGroupObject ));    
      };  
    });
  }


  // Code for an Modal Window
  // https://www.mediawiki.org/wiki/Snippets/Modals_in_Mediawiki
  function onEditSecretRightsClick(){
    var aModal   =  ' <div class="pws_modal pws_modal_dialog">'
                 +  '   <div>'
                 +  '     <a href="#close2" title="Close" class="pws_modal_close">X</a>'
                 +  '     <div style="max-height:500px;overflow:auto;">'
                 +  '       <h4>Informations about the secret: <strong>' + this.secretKey + '</strong></h4>'
                 +  '       <h4>Allowed Readers</h4>';
    if (this.secretEditable){
        aModal   += '       <input type="text" class="pws_select_reader pws_input" placeholder="Search for Users or Groups to give read access."></input>';
    } else {
        aModal   += '       <p class="pws-warning">You have no permission to read this secret!</p>';
    }    
        aModal   += '       <ul class="allowedReaders plainList">'
                 +  '         <li>No Readers present.</li>'
                 +  '       </ul>'
                 +  '       <h4>Allowed Editors</h4>';
    if (this.secretEditable){
        aModal   += '       <input type="text" class="pws_select_editor pws_input" placeholder="Search for Users or Groups to give edit access."></input>';
    } else {
        aModal   += '       <p class="pws-warning">You have no permission to edit this secret!</p>';
    }    
        aModal   += '       <ul class="allowedEditors plainList">'
                 +  '         <li>No Editors present.</li>'
                 +  '       </ul>'
                 +  '     </div>'
                 +  '   </div>'
                 +  ' </div>';
    $('body').append(aModal);   
    $('.pws_modal_close').click(function(){
      $('div.pws_modal').remove();
    });
    // this equals aSecretObject           
    buildUserAutoCompleteStuff(this);
    buildEditorAutoCompleteStuff(this);
    insertAllowedReaders(this);
    insertAllowedEditors(this);
  }    

  
  function onRemoveReaderClick(anUIEvent){
    // this is equal to aSecretObject
    var readerId = $(anUIEvent.target).data('reader_id');
    var groupId  = $(anUIEvent.target).data('group_id'); 
    if (( readerId != null ) || ( groupId != null )) {
      var anURL = '';
      if (readerId != null) {
        anURL = urlPrefix + "/api.php?action=PassWordSafe&method=removeUserRight&secretKey=" + this.secretKey + "&readerId=" + readerId + "&rightType=0&format=json";   
      } else {
        anURL = urlPrefix + "/api.php?action=PassWordSafe&method=removeUserRight&secretKey=" + this.secretKey + "&groupId=" + groupId + "&rightType=0&format=json";      
      }
      if (confirm('Do You realy want to remove this read Access?' )) {       
        $.ajax({
          url :    anURL,
          context: this
        }).done( function(data) {      
          insertAllowedReaders(this);
        })
      }
    }
  }

  
  function onRemoveEditorClick(anUIEvent){
    // this is equal to aSecretObject
    var editorId = $(anUIEvent.target).data('editor_id');
    var groupId  = $(anUIEvent.target).data('group_id'); 
    if (( editorId != null ) || ( groupId != null )) {
      var anURL = '';
      if (editorId != null) {
        anURL = urlPrefix + "/api.php?action=PassWordSafe&method=removeUserRight&secretKey=" + this.secretKey + "&readerId=" + editorId + "&rightType=1&format=json";   
      } else {
        anURL = urlPrefix + "/api.php?action=PassWordSafe&method=removeUserRight&secretKey=" + this.secretKey + "&groupId="  + groupId + "&rightType=1&format=json";      
      }
      if (confirm('Do You realy want to remove this edit access?' )) {       
        $.ajax({
          url :    anURL,
          context: this
        }).done( function(data) {      
          insertAllowedEditors(this);
        })
      }
    }
  }


  function insertAllowedReaders(aSecretObject){
    $.ajax({
      url:     urlPrefix + "/api.php?action=PassWordSafe&method=getAllowedReaders&secretKey=" + aSecretObject.secretKey + "&format=json",
      context: aSecretObject
    }).done( function(data) {     
      if ((data != null) && (data.PassWordSafe != null)){
        var readerTags  = '';
        for (var a = 0; a < data.PassWordSafe.allowedReaders.length; a++){
          aReader       = data.PassWordSafe.allowedReaders[a];
          readerTags   += '<li>';
          if (aReader.isGroup == 1){
            readerTags += '<a href="#" class="pws_group" title="Group of Users">'
                       +  '<a href="#">' + aReader.name + '</a>';
          } else {
            readerTags += '<a href="#" class="pws_user" title="Single User">'
                       +  '<a href="' + urlPrefix + '/index.php?title=User:' + aReader.user_name + '">' + aReader.user_name + '</a>';
          }             
          // You can not remove the Owner of a Secret
          if ((aSecretObject.secretEditable) && (aReader.isGroup == 0) && (aSecretObject.secretOwnerId != aReader.user_id)){           
            readerTags +=   '<a href="#" class="removeReader pws_trash" data-reader_id="' + aReader.user_id + '" title="Remove Reader" /a>';
          }           
          if ((aSecretObject.secretEditable) && (aReader.isGroup == 1)){           
            readerTags +=   '<a href="#" class="removeReader pws_trash" data-group_id="' + aReader.id + '" title="Remove Group of Readers" /a>';
          }           

            readerTags += '</li>';
        }
        $('ul.allowedReaders').html(readerTags);
        $('a.removeReader').click($.proxy(onRemoveReaderClick, this));    
      }         
    }); 
  }

  function insertAllowedEditors(aSecretObject){
    $.ajax({
      url:     urlPrefix + "/api.php?action=PassWordSafe&method=getAllowedEditors&secretKey=" + aSecretObject.secretKey + "&format=json",
      context: aSecretObject
    }).done( function(data) {       
      if ((data != null) && (data.PassWordSafe != null)){        
        var editorTags   = '';
        for (var a = 0; a < data.PassWordSafe.allowedEditors.length; a++){
          editorTags    += '<li>';  
          anEditor       = data.PassWordSafe.allowedEditors[a];
          if (anEditor.isGroup == 1){
            editorTags  += '<a href="#" class="pws_group" title="Group of Users" />'
                        +  '<a href="#">' + anEditor.name + '</a>';
          } else {
            editorTags  += '<a href="#" class="pws_user" title="Single User" />'
                        +  '<a href="' + urlPrefix + '/index.php?title=User:' + anEditor.user_name + '">' + anEditor.user_name + '</a>';
          }            
          // You can not remove the Owner of a Secret
          if ((aSecretObject.secretEditable) && (anEditor.isGroup == 0) && (aSecretObject.secretOwnerId != anEditor.user_id)){           
            editorTags  +=   '<a href="#" class="removeEditor pws_trash" data-editor_id="' + anEditor.user_id + '" title="Remove Editor" />';
          }    
          if ((aSecretObject.secretEditable) && (anEditor.isGroup == 1) ){           
            editorTags  +=   '<a href="#" class="removeEditor pws_trash" data-group_id="' + anEditor.id + '" title="Remove Group of Editors" />';
          }
          editorTags    += '</li>';
        }
        $('ul.allowedEditors').html(editorTags);
        $('a.removeEditor').click($.proxy(onRemoveEditorClick, this));        
      }         
    }); 
  }

  function renderGroups(someGroups){
    var groupTags = '<ul class="plainList" >';
    groups        = [];
    for (var a = 0; a < someGroups.length; a++){
      // Storing group credentials in a gloabl array by ID
      var aGroup        = someGroups[a];
      if (someGroups[a].editable){ aGroup.editable = true } else { aGroup.editable = false }
      groups[aGroup.id] = aGroup;
      groupTags    +=   '<li>';
      groupTags    +=     '<a href="#" class="pws_group">';
      groupTags    +=     '<a href="#" class="showGroupDetails" data-group_id="' + aGroup.id + '" title="Click to View or Edit." >' + aGroup.name + '</a>';
      if (aGroup.editable == true){
        groupTags  +=       '<a href="#" class="removeGroup pws_trash" data-group_id="' + aGroup.id + '" title="Remove Group" />';
      }
      groupTags    +=   '</li>';
    }
    groupTags      += '</ul>';
    $('div.pws_group_list').html(groupTags);
    $('a.removeGroup').click(onRemoveGroupClick); 
    $('a.showGroupDetails').click(showGroupInformations);
  }


  function onRemoveGroupClick(anUIEvent){
    anUIEvent.preventDefault();
    var aGroupId = $(anUIEvent.target).data('group_id');
    if (confirm('Do You realy want to remove this group and all related access rights?' )) { 
      $.ajax({
        url : urlPrefix + "/api.php?action=PassWordSafe&method=removeGroup&groupId=" + aGroupId + "&format=json",
      }).done(function(data){
        getGroups();
      });
    }  
  };


  function onRemoveGroupEditorClick(anUIEvent){
    anEditorId = $(anUIEvent.target).data('editor_id');
    if (confirm('Do You realy want to remove this editor from the group?' )) { 
      $.ajax({
        url     : urlPrefix + "/api.php?action=PassWordSafe&method=removeGroupEditor&groupId=" + this.id + "&editorId=" + anEditorId + "&format=json",
        context : this
      }).done(function(data){
        insertAllowedGroupEditors(this);
      });
    }  
  }


  function onRemoveGroupMemberClick(anUIEvent){
    aMemberId = $(anUIEvent.target).data('member_id');
    if (confirm('Do You realy want to remove this member from the group?' )) { 
      $.ajax({
        url     : urlPrefix + "/api.php?action=PassWordSafe&method=removeGroupMember&groupId=" + this.id + "&memberId=" + aMemberId + "&format=json",
        context : this
      }).done(function(data){
        insertGroupMembers(this);
      });
    }  
  }

  



  // Build with the help of this
  // http://api.jqueryui.com/autocomplete/#option-source
  function buildUserAutoCompleteStuff(aSecretObject){
    if (aSecretObject.secretEditable){
      $( "input.pws_select_reader" ).autocomplete({
        source: function( aRequest, aResponse ) {
          $.ajax({
            url:      urlPrefix + "/api.php?action=PassWordSafe&method=getUsersByKeyStroke&secretKey=" + aSecretObject.secretKey + "&keyStrokes=" + aRequest.term + "&format=json",
            context : aSecretObject,
            success: function( data ) {
              var aResult = [];
              if ((data != null) && (data.PassWordSafe != null)){           
                aResult = data.PassWordSafe.foundUsers;
              };
              aResponse( aResult );
            }
          });
        },
        minLength: 1,
        select: function( event, ui ) {
          var anURL = '';
          if (ui.item.isGroup == 1){
            anURL = urlPrefix + "/api.php?action=PassWordSafe&method=setUserRight&secretKey=" + ui.item.secretKey + "&groupId=" + ui.item.groupId + "&rightType=0&format=json";
          } else {
            anURL = urlPrefix + "/api.php?action=PassWordSafe&method=setUserRight&secretKey=" + ui.item.secretKey + "&readerId=" + ui.item.user_id + "&rightType=0&format=json";
          }
          $.ajax({
            url     : anURL, 
            context : secretKeys[ui.item.secretKey],
            success : function( data ) {
              insertAllowedReaders(this);
            }
          });
          $( "input.pws_select_reader" ).val('');
        },
        open: function() {
          $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
          $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
          $( "input.pws_select_reader" ).val('');
        }
      }).data( "autocomplete" )._renderItem = function( ul, item ) {
        if (item.isGroup == 1){
          return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>")            
            .appendTo( ul );
        } else {
          return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + " (" + item.user_real_name + ")</a>")    
            .appendTo( ul );
        }    
      };  
    }
  }      

  function buildEditorAutoCompleteStuff(aSecretObject){
    if (aSecretObject.secretEditable){
      $( "input.pws_select_editor" ).autocomplete({
        source: function( aRequest, aResponse ) {
          $.ajax({
            url:      urlPrefix + "/api.php?action=PassWordSafe&method=getUsersByKeyStroke&secretKey=" + aSecretObject.secretKey + "&keyStrokes=" + aRequest.term + "&format=json",
            context : aSecretObject,
            success: function( data ) {
              var aResult = [];
              if ((data != null) && (data.PassWordSafe != null)){           
                aResult = data.PassWordSafe.foundUsers;
              };
              aResponse( aResult );
            }
          });
        },
        minLength: 1,
        select: function( event, ui ) {
          var anURL = '';
          if (ui.item.isGroup == 1){
            anURL = urlPrefix + "/api.php?action=PassWordSafe&method=setUserRight&secretKey=" + ui.item.secretKey + "&groupId=" + ui.item.groupId + "&rightType=1&format=json";
          } else {
            anURL = urlPrefix + "/api.php?action=PassWordSafe&method=setUserRight&secretKey=" + ui.item.secretKey + "&readerId=" + ui.item.user_id + "&rightType=1&format=json";
          }
          $.ajax({
            url     : anURL, 
            context : secretKeys[ui.item.secretKey],
            success : function( data ) {
              insertAllowedEditors(this);
            }
          });
          $( "input.pws_select_editor" ).val('');
        },
        open: function() {
          $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
          $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
          $( "input.pws_select_editor" ).val('');
        }
      }).data( "autocomplete" )._renderItem = function( ul, item ) {
        if (item.isGroup == 1){
          return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>")            
            .appendTo( ul );
        } else {
          return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + " (" + item.user_real_name + ")</a>")    
            .appendTo( ul );
        }    
      };  
    }
  }      


  function buildGroupEditingAutoCompleteStuff(aGroupObject){
    if (aGroupObject.editable == true){
      // The Editors
      $( "input.pws_select_group_editor" ).autocomplete({
        source: function( aRequest, aResponse ) {
          $.ajax({
            url:      urlPrefix + "/api.php?action=PassWordSafe&method=getUsersByKeyStroke&groupId=" + aGroupObject.id + "&keyStrokes=" + aRequest.term + "&format=json",
            context : aGroupObject,
            success: function( data ) {
              var aResult = [];
              if ((data != null) && (data.PassWordSafe != null)){           
                aResult = data.PassWordSafe.foundUsers;
              };
              aResponse( aResult );
            }
          });
        },
        minLength: 1,
        select: function( event, ui ) {
          $.ajax({
            url     : urlPrefix + "/api.php?action=PassWordSafe&method=setGroupEditor&groupId=" + ui.item.groupId + "&editorId=" + ui.item.user_id + "&format=json", 
            context : groups[ui.item.groupId],
            success : function( data ) {
              insertAllowedGroupEditors(this);              
            }
          });
          $( "input.pws_select_group_editor" ).val('');
        },
        open: function() {
          $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
          $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
          $( "input.pws_select_group_editor" ).val('');
        }
      }).data( "autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
          .data( "item.autocomplete", item )
          .append( "<a>" + item.label + " (" + item.user_real_name + ")</a>" )
          .appendTo( ul );
      };
      // The Members
      $( "input.pws_select_group_member" ).autocomplete({
        source: function( aRequest, aResponse ) {
          $.ajax({
            url:      urlPrefix + "/api.php?action=PassWordSafe&method=getUsersByKeyStroke&groupId=" + aGroupObject.id + "&keyStrokes=" + aRequest.term + "&format=json",
            context : aGroupObject,
            success: function( data ) {
              var aResult = [];
              if ((data != null) && (data.PassWordSafe != null)){           
                aResult = data.PassWordSafe.foundUsers;
              };
              aResponse( aResult );
            }
          });
        },
        minLength: 1,
        select: function( event, ui ) {
          $.ajax({
            url     : urlPrefix + "/api.php?action=PassWordSafe&method=setGroupMember&groupId=" + ui.item.groupId + "&memberId=" + ui.item.user_id + "&format=json", 
            context : groups[ui.item.groupId],
            success : function( data ) {
              insertGroupMembers(this);
            }
          });
          $( "input.pws_select_group_member" ).val('');
        },
        open: function() {
          $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
          $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
          $( "input.pws_select_group_member" ).val('');
        }
      }).data( "autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
          .data( "item.autocomplete", item )
          .append( "<a>" + item.label + " (" + item.user_real_name + ")</a>" )
          .appendTo( ul );
      };
    }
  }


  function showError(aSecretObject, anError){
    $(aSecretObject.secretElement).html(
        '<a href="#" class="pws-lock show_secret_rights" title="' + anError + '">'
    );
    $(aSecretObject.secretElement).find('a.show_secret_rights').click($.proxy(onEditSecretRightsClick, aSecretObject)); 
  }

  // will be called after clicking the create Button
  function onSecretCreateClick(anUIEvent){
    // this equals aSecretObject here, its exposed by the jquery proxy function 
    this.secretItSelf = $(this.secretElement).find('input').val();
    if (this.secretItSelf.length >= 2) { 
      setSecret(this);
    }
  };


  // Will create the Secret when seen first Time
  function createSecretForTheFirstTime(aSecretObject){
    var anSecretLabel      = '<span>Please create new Secret.</span><br>';
    var anSecretInput      = '<input  class="secret pws_secret" type="text" placeholder="Please type secret here."></input>';
    var anSubMitButton     = '<button class="create_new_secret">Submit</button>';    
    $(aSecretObject.secretElement).html(anSecretLabel + anSecretInput + anSubMitButton);
    $(aSecretObject.secretElement).find('button.create_new_secret').click($.proxy(onSecretCreateClick, aSecretObject));
  }


  function checkSecretExists(aSecretObject){
    $.ajax({
      url:     urlPrefix + "/api.php?action=PassWordSafe&method=secretExists&secretKey=" + aSecretObject.secretKey + "&format=json",
      context: aSecretObject
    }).done( function(data) {      
      if ((data != null) && (data.PassWordSafe != null)){        
        this.secretExists   = Boolean(data.PassWordSafe.secretExists);
        this.secretViewable = Boolean(data.PassWordSafe.secretViewable);
        this.secretEditable = Boolean(data.PassWordSafe.secretEditable);
        this.secretOwnerId  = data.PassWordSafe.secretOwnerId;
      } 
      renderSecret(this);      
    });  
  }

  
  function setSecret(aSecretObject){
    $.ajax({
      url:     urlPrefix + "/api.php?action=PassWordSafe&method=setSecret&secretKey=" + aSecretObject.secretKey + "&encodedSecret=" + Base64.encode(aSecretObject.secretItSelf) + "&format=json",
      context: aSecretObject
    }).done(function(data){
      checkSecretExists(this);
    });
  }
  

  function getSecret(aSecretObject){
    $.ajax({
      url:     urlPrefix + "/api.php?action=PassWordSafe&method=getSecret&secretKey=" + aSecretObject.secretKey + "&format=json",
      context: aSecretObject
    }).done(function(data){      
      if ((data != null) && (data.PassWordSafe != null)){
        this.secretItSelf = Base64.decode(data.PassWordSafe.secretItSelf);
      }  
      showSecret(this);
    });
  }


  function getGroups(){
    $.ajax({
      url: urlPrefix + "/api.php?action=PassWordSafe&method=getGroups&format=json",
    }).done(function(data){
      if ((data != null) && (data.PassWordSafe != null)){
        renderGroups(data.PassWordSafe.allGroups);
      };
    });
  }


  // This has nothing to do with PWS ist just a special case for the IETWiki
  if ($('button.ssh-public-key').length > 0){    
    $('button.ssh-public-key').click( showPublicKey );
  }
  

  function showPublicKey(anUIEvent){
    anUIEvent.preventDefault();
    thePublicKey = $('button.ssh-public-key').data('public-key').trim()    
    var aModal   = '<div class="pws_modal pws_modal_dialog" >'
                 + '  <div>'
                 + '    <a href="#close2" title="Close" class="pws_modal_close">X</a>'
                 + '    <div style="max-height:500px;overflow:auto;">'
                 + '      <h3>Details to the public key</h3>'
                 + '      <textarea class="public-key" rows="6" cols="80">' + thePublicKey + '</textarea>'
                 + '    </div>'
                 + '  </div>'  
                 + '</div>'; 
    $('body').append(aModal);
    $('.pws_modal_close').click(function(){
      $('div.pws_modal').remove();
    });
  }


}( mediaWiki, jQuery ) );