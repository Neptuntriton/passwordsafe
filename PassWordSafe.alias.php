<?php
/**
 * Aliases for PassWordSafe
 *
 * @file
 * @ingroup Extensions
 */

$specialPageAliases = array();

/** English
 * @author neptuntriton
 */
$specialPageAliases['en'] = array(
  'PassWordSafe' => array( 'PassWordSafe', 'Password Safe', 'Secrets Box' ),
);

/** Deutsch
 * @author neptuntriton
 */
$specialPageAliases['de'] = array(
  'MyExtension' => array( 'PassWortSafe', 'Passwort-Safe', 'Geheimnisbox' ),
);