SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE TABLE IF NOT EXISTS `pws_secrets` (
  `id`       int(5)                                               NOT NULL AUTO_INCREMENT COMMENT 'Just a Number',
  `pw_key`   varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL                COMMENT 'key to the secret',
  `secret`   varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL                COMMENT 'the secret itself',
  `owner`    int(5)                                               NOT NULL                COMMENT 'Wiki user id of the creator of the secret',
  PRIMARY KEY (`pw_key`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores the Secrets belonging to the keys!' AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `pws_groups` (
  `id`       int(5)                                               NOT NULL AUTO_INCREMENT COMMENT 'id of this group',
  `name`     varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL                COMMENT 'name of the group',
  `owner`    int(5)                                               NOT NULL                COMMENT 'Wiki user id of creator of this group',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `pws_rights` (
  `id`         int(5) NOT NULL AUTO_INCREMENT COMMENT 'id of this right',
  `secret_id`  int(5) NOT NULL                COMMENT 'id to the secret this right belongs to',
  `user_id`    int(5) NOT NULL                COMMENT 'wiki user id this right belongs to',
  `group_id`   int(5) NOT NULL                COMMENT 'pws group id this right belongs to',
  `right_type` int(5) NOT NULL DEFAULT '0'    COMMENT '0 -> just reading; 1 reading and editing',
  PRIMARY KEY (  `secret_id` ,  `user_id` ,  `group_id` ,  `right_type` ),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `pws_user_of_group` (
  `group_id`   int(5) NOT NULL                COMMENT 'id of related pws_group',
  `user_id`    int(5) NOT NULL                COMMENT 'id_of related wiki_user',
  PRIMARY KEY (`group_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Relates wiki_users to groups';


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;