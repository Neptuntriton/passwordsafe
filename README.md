# README #

This is an extension to Mediawiki. The Extension provides a method to store secerts in Wiki Pages and Control the access to it.

### Prerequisites ###
* Mediawiki Version 1.23 or above (Tested with it)

### Installation ###

* Log in to your Wiki Server
* Navigate to your Extensions Folder
* Clone this Extension using git clone
* Raname the Folder from passwordsafe to PassWordSafe
* Edit LocalSettings.php and add the extension
* navigate to the maintenance Folder of your Wiki and execute the update.php. This will create the DataBase Tables needed to run this extension.

### Usage ###

* Place a secret Tag in your Wiki Code. 
* Embed the Key to Your Secret in it.
* Save changes of the Page.
* Enter the secret into the input Box.
* Save the secret.
* Add access to diffrent users if needed.

``` html
  <secret>KeyToMySecret</secret>
```

### Console Commands Installation ###

``` bash

  ssh -l user your_server_domain_name
  cd path_to_your_wiki
  cd extensions
  git clone https://Neptuntriton@bitbucket.org/Neptuntriton/passwordsafe.git
  mv passwordsafe PassWordSafe
  cd ..
  # Using an Editor to add extension to LocalSettings
  nano LocalSettings.php
  cd maintenance
  php update.php

```