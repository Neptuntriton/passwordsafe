<?php

/**
 * MediaWiki PassWordSafe extension
 *
 * @file
 * @ingroup   Extensions
 * @version   1.0
 * @author    Jens Haupt
 * @copyright © 2015-2016 neptuntriton (Jens Haupt)
 * @license   GPLv2 license; info in main package.
 * @link      http://www.mediawiki.org/wiki/Extension:PassWordSafe 
 */

if ( !defined( 'MEDIAWIKI' ) ) {
  die( "This is not a valid entry point to MediaWiki.\n" );
}

// Extension credits that will show up on Special:Version
$wgExtensionCredits['parserhook'][] = array (
  'path'    => __FILE__,
  'name'    => 'PassWordSafe',
  'version' => '0.0.9',
  'author'  => array(
    'Jens Haupt',
  ),
  'descriptionmsg' => 'description',
  'url'            => 'https://www.mediawiki.org/wiki/Extension:PassWordSafe',
);


function deb($logmessage){

  #file_put_contents("pws.log", $logmessage."\n", FILE_APPEND);  

}

// We want to use the Wiki Logger only from 1.25 on
#use MediaWiki\Logger\LoggerFactory;

# ************************ API Stuff ********************************************
# We Load this class from File Automatically
# It extends the core Wiki API by Functions exclusive for this Extension
# i.e. We you can call: https://Hostname/WikiName/api.php?action=PassWordSafe&method=getAllowedViewers&secretKey=MyPassword 
# Read More About this here: https://www.mediawiki.org/wiki/API:Extensions
$wgAutoloadClasses['PassWordSafeApi']     = __DIR__ . '/PassWordSafeApiExtension.php';
# Map module name to class name
$wgAPIModules['PassWordSafe']             = 'PassWordSafeApi';
# Here we have our language Files
$wgMessagesDirs['PassWordSafe']           = __DIR__ . '/i18n'; 


# ************************ Special Page Stuff *******************************************
# We Load this class from File Automatically
$wgAutoloadClasses['SpecialPassWordSafe']      = __DIR__ . '/SpecialPassWordSafe.php';
# Tell MediaWiki about the new special page and its class name
$wgSpecialPages['PassWordSafe']                = 'SpecialPassWordSafe'; 
# Location of an aliases file (Tell MediaWiki to load it)
$wgExtensionMessagesFiles['PassWordSafeAlias'] = __DIR__ . '/PassWordSafe.alias.php'; 


# ************************ Hooks *******************************************
# This functions are called before the parser runs the first Time
$wgHooks['ParserFirstCallInit'][]         = 'PassWordSafe_onParserFirstCallInit';
# This functions are called before the page is displayed
$wgHooks['BeforePageDisplay'][]           = 'PassWordSafeOnBeforePageDisplay';
# This function is called by Update Script to install the new Tables needed by this extension
$wgHooks['LoadExtensionSchemaUpdates'][]  = 'PassWordSafeOnLoadExtensionSchemaUpdates';


# We load the related js File
$wgResourceModules['ext.pws.base64'] = array(
  'localBasePath' => __DIR__ . '/script',
  'remoteExtPath' => 'PassWordSafe/script',
  'scripts'       => 'ext.base64.js',
);

# We load the related js File
$wgResourceModules['ext.pws.scripts'] = array(
  'localBasePath' => __DIR__ ,
  'remoteExtPath' => 'PassWordSafe/script',
  'scripts'       => 'script/ext.pws.js',
  'styles'        => 'style/pws.css',
  'dependencies'  => array( 'ext.pws.base64' ),
);



# We add our Scripts to the Page
function PassWordSafeOnBeforePageDisplay(OutputPage &$out, Skin &$skin) {
  $out->addModules( 'ext.pws.base64' );
  $out->addModules( 'ext.pws.scripts' );
  return true;
};



// Register the secret Tag Function
function PassWordSafe_onParserFirstCallInit( $parser ) {
  $parser->setHook( 'secret', 'secretTagHook' );

  deb("Secret Hook Registered Properly.");
  #LoggerFactory::getInstance( 'PassWordSafe' )->info( "Secret Hook Registered Properly." );

  return true;  
}



function secretTagHook( $content, $attributes, $parser ) {
  
  if ( trim( $content ) === '' ) { // bug 8372
    return '';
  }

  # We only Place a marker on the Page
  # Here all Function will be added by JavaScript
  $renderedSecret = "<div class='secret' id='".$content."' data-secret='".$content."'>Hier steht das Passwort</div>";
  
  #LoggerFactory::getInstance( 'PassWordSafe' )->info( "Function secretTagHook called." );

  return array( $renderedSecret, "markerType" => 'nowiki' );
}


# Further Infos can be found here
# This is called on php update.php
# https://www.mediawiki.org/wiki/Manual:Hooks/LoadExtensionSchemaUpdates
function PassWordSafeOnLoadExtensionSchemaUpdates( $updater = null ){
    
    deb('Try to build Database for PassWordSafe');

    if ( is_null( $updater ) ) {
      throw new Exception( 'PassWordSafe extension is only necessary in 1.18 or above' );
    }

    $map  = array( 'mysql' );
    $type = $updater->getDB()->getType();

    if ( !in_array( $type, $map ) ) {
      throw new Exception( "PassWordSafe extension does not currently support $type database." );
    }

    $sql = __DIR__ . '/db/PassWordSafe.' . $type . '.sql';
    $updater->addExtensionTable( 'PassWordSafe', $sql );

    return true; 
}


/*
  List of Allowed ApiCalls

  * Secret Exists
  https://Hostname/WikiName/api.php?action=PassWordSafe&method=secretExists&secretKey=MyPassword
  
  * Get Allowed Viewers
  https://Hostname/WikiName/api.php?action=PassWordSafe&method=getAllowedViewers&secretKey=MyPassword

  * Get Secret
  https://Hostname/WikiName/api.php?action=PassWordSafe&method=getSecret&secretKey=MyPassword

  * Set Secret
  https://Hostname/WikiName/api.php?action=PassWordSafe&method=setSecret&secretKey=MyPassword&ownerId=2&encodedSecret=APassWord

  Manage Secrets
  
  * Get All Groups
  https://Hostname/WikiName/api.php?action=PassWordSafe&method=getGroups

  * Get All Users of a Group
  https://Hostname/WikiName/api.php?action=PassWordSafe&method=getUsersOfGroup&groupId=2


  You can append an Format option
  format - The format of the output
          One value: json, jsonfm, php, phpfm, wddx, wddxfm, xml, xmlfm, yaml, yamlfm, rawfm, txt, txtfm, dbg, dbgfm,
                     dump, dumpfm, none
          Default: xmlfm

*/
