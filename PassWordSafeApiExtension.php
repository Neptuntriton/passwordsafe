<?php
class PassWordSafeApi extends ApiBase {


  public function execute() {
    
    
    # We want the User Object to be accessible within our class
    global $wgUser;


    # Obtaining an Database Object for Reading
    # $dbr = wfGetDB( DB_SLAVE ); 
    # $res = $dbr->select( /* ...see docs... */ );
    # foreach( $res as $row ) { 
    #   ...
    # }

    # Obtaining an Database Object for Writing
    # $dbw = wfGetDB( DB_MASTER ); 

    # In Result to the method we have diffrent tasks to do
    $method = $this->getMain()->getVal( 'method' );
    switch ($method) {
      

      case 'secretExists' :
        deb('secretExists method called');
        $secretKey     = $this->getMain()->getVal( 'secretKey' );        
        $secretId      = $this->getIdOfSecret($secretKey);
        $secretOwnerId = $this->getOwnerIdOfSecret($secretId);
        $wikiUserId    = $wgUser->getId();
        #deb($wgUser->getId());
       
        $this->getResult()->addValue( 
          null, 
          $this->getModuleName(),
          array ( 
            'secretExists'   => $this->Bool2Num($this->secretExists(     $secretKey )),
            'secretViewable' => $this->Bool2Num($this->isViewableByUser( $secretId,  $wikiUserId )),
            'secretEditable' => $this->Bool2Num($this->isEditableByUser( $secretId,  $wikiUserId )),
            'secretOwnerId'  => $secretOwnerId
          ) 
        );
        break;  


      case 'getSecret' :
        deb('getSecret method called');
        $secretItSelf = "???"; 
        $secretKey    = $this->getMain()->getVal( 'secretKey' );
        $wikiUserId   = $wgUser->getId();
        $secretId     = $this->getIdOfSecret($secretKey);
        if ($this->isViewableByUser($secretId, $wikiUserId )) {
          $dbr = wfGetDB( DB_SLAVE );          
          $row = $dbr->selectRow('pws_secrets',
                                 array('secret'),
                                 "id = '$secretId'",
                                 '',
                                 '');
          if ($row != false){ $secretItSelf = $row->secret; }                                  
        };
        $this->getResult()->addValue( 
          null, 
          $this->getModuleName(),
          array ( 
            'secretItSelf'   => base64_encode($secretItSelf)
          ) 
        );
        break;


      case 'setSecret' :
        deb('setSecret method called');
        $secretKey     = $this->getMain()->getVal( 'secretKey' );
        $ownerId       = $wgUser->getId();
        $encodedSecret = $this->getMain()->getVal( 'encodedSecret' );
        $decodedSecret = base64_decode($encodedSecret);
        if (($secretKey != null) && ($encodedSecret != null) && ($ownerId != null) && ($ownerId > 0)) {
          $dbw =  wfGetDB( DB_MASTER );
          $dbw->insert( 'pws_secrets', array( 'pw_key' => $secretKey, 'secret' => $decodedSecret, 'owner' => $ownerId ));
        } else {
          $this->getResult()->addValue(null, $this->getModuleName(), array( 'error' => 'at least 1 paramter missing' ));
        } 
        break;


      case 'getUsersByKeyStroke' :
        deb('getUsersByKeyStroke method called');
        // for strings conatining strings we have to use LIKE statement
        // http://stackoverflow.com/questions/2602252/mysql-query-string-contains
        // ToDo: exclude users with read or edit rights
        $secretKey  = $this->getMain()->getVal( 'secretKey'  );
        $keyStrokes = $this->getMain()->getVal( 'keyStrokes' );
        $groupId    = $this->getMain()->getVal( 'groupId'    );
        $foundUsers = array();
        $dbr  = wfGetDB( DB_SLAVE );          
        $rows = $dbr->select('user',
                              array('user_id', 'user_name', 'user_real_name' ),
                              "user_name LIKE '%{$keyStrokes}%' OR user_real_name LIKE '%{$keyStrokes}%'",
                              '',
                              'LIMIT 8');
        foreach( $rows as $row ) { 
          $row->label     = $row->user_name;
          $row->secretKey = $secretKey;
          $row->groupId   = $groupId;
          $row->isGroup   = 0;
          $foundUsers[]   = $row;
        }
        // here we also want to find groups
        if ($groupId == null){
          deb('getUsersByKeyStroke with Groups');
          $rows = $dbr->select('pws_groups',
                                array('id', 'name'),
                                "name LIKE '%{$keyStrokes}%'",
                                '',
                                'LIMIT 4');
          foreach( $rows as $row ) {           
            $row->label     = $row->name;
            $row->secretKey = $secretKey;
            $row->groupId   = $row->id;
            $row->isGroup   = 1;
            $foundUsers[]   = $row;
          }
        }
        $this->getResult()->addValue(  
          null, 
          $this->getModuleName(),
          array ( 
            'foundUsers'  => $foundUsers
          ) 
        );
        break;  


      case 'setUserRight' : 
        deb('setUserRight method called');
        $secretKey  = $this->getMain()->getVal( 'secretKey' );
        $readerId   = $this->getMain()->getVal( 'readerId'  );
        $groupId    = $this->getMain()->getVal( 'groupId'   );
        $rightType  = $this->getMain()->getVal( 'rightType' );
        $secretId   = $this->getIdOfSecret($secretKey);
        $wikiUserId = $wgUser->getId();        
        if (($rightType != null) && ($secretId > -1)  && ($wikiUserId != null) && ($wikiUserId > 0)){
          if ($this->isEditableByUser( $secretId,  $wikiUserId )){
            // Adding a Right to a User
            if (($readerId != null) && ($groupId == null) && ($readerId > 0)){
              #deb('Adding a Right to a User');
              $dbw        = wfGetDB( DB_MASTER );
              $dbw->insert( 'pws_rights', array( 'secret_id' => $secretId, 'group_id' => -1, 'user_id' => $readerId, 'right_type' => $rightType ));
              #deb('|-> inserted');  
            }  
            // Adding a Right to a Group
            if (($readerId == null) && ($groupId != null) && ($groupId > 0)){
              #deb('Adding a Right to a Group');
              $dbw        = wfGetDB( DB_MASTER );
              $dbw->insert( 'pws_rights', array( 'secret_id' => $secretId, 'group_id' => $groupId, 'user_id' => -1, 'right_type' => $rightType ));
              #deb('|-> inserted');  
            }
          }  
        }               
        break;


      case 'removeUserRight' :
        deb('removeUserRight method called');
        $secretKey        = $this->getMain()->getVal( 'secretKey' );        
        $readerId         = $this->getMain()->getVal( 'readerId'  ); 
        $groupId          = $this->getMain()->getVal( 'groupId'   );
        $rightType        = $this->getMain()->getVal( 'rightType' );
        $secretId         = $this->getIdOfSecret($secretKey); 
        $wikiUserId       = $wgUser->getId();
        if (($rightType != null) && ($secretId > -1)){
          if ($this->isEditableByUser( $secretId,  $wikiUserId )){
            if (($readerId != null ) || ($groupId != null )){
              $dbr = wfGetDB( DB_MASTER);
              if ($readerId != null) {
                $dbr->delete('pws_rights',"user_id  = '$readerId' AND secret_id = '$secretId' AND group_id = -1 AND right_type = '$rightType' ",'');               
              } else {
                $dbr->delete('pws_rights',"group_id = '$groupId'  AND secret_id = '$secretId' AND user_id  = -1 AND right_type = '$rightType' ",'');               
              }
            }
          }
        }     
        break; 


      case 'getAllowedReaders' :
        deb('getAllowedReaders method called');
        $wikiUserId       = $wgUser->getId();
        $secretKey        = $this->getMain()->getVal( 'secretKey' );
        $secretId         = $this->getIdOfSecret($secretKey);
        $ownerId          = $this->getOwnerIdOfSecret($secretId);
        $allowedReaderIds = '(';
        $allowedGroupIds  = '(';  
        $allowedReaders   = array(); 
        if ( ($wikiUserId != null) && ($wikiUserId > 0) && ($secretId > -1)) {
          // Search for single Users
          $dbr  = wfGetDB( DB_SLAVE);
          $rows = $dbr->select('pws_rights',
                               array('user_id'),
                               "secret_id = '$secretId' AND group_id = -1 AND right_type = 0",
                               '',
                               '');
          foreach( $rows as $row ) { 
            $allowedReaderIds .= $row->user_id.', ' ;
          }
          $allowedReaderIds .= $ownerId.')';          
          $rows = $dbr->select('user',
                              array('user_id', 'user_name', 'user_real_name' ),
                              "user_id IN ".$allowedReaderIds,
                              '',
                              '');
          foreach( $rows as $row ) {
            $row->isGroup     = 0; 
            $allowedReaders[] = $row;            
          }
          // Search for Groups
          $rows = $dbr->select('pws_rights',
                               array('group_id'),
                               "secret_id = '$secretId' AND user_id = -1 AND right_type = 0",
                               '',
                               '');
          foreach( $rows as $row ) { 
            $allowedGroupIds .= $row->group_id.', ';
          }
          $allowedGroupIds .= ' 0)';
          $rows = $dbr->select('pws_groups',
                               array('id', 'name'),
                               "id IN ".$allowedGroupIds,
                               '',
                               '');          
          foreach( $rows as $row ) {
            $row->isGroup     = 1; 
            $allowedReaders[] = $row;            
          }          
        }
        $this->getResult()->addValue(  
          null, 
          $this->getModuleName(),
          array ( 
            'allowedReaders'  => $allowedReaders
          ) 
        );       
        break;  


      case 'getAllowedEditors' :
        deb('getAllowedEditors called');
        $wikiUserId       = $wgUser->getId();
        $secretKey        = $this->getMain()->getVal( 'secretKey' );
        $secretId         = $this->getIdOfSecret($secretKey);
        $ownerId          = $this->getOwnerIdOfSecret($secretId);
        $allowedEditorIds = '(';
        $allowedGroupIds  = '(';           
        $allowedEditors   = array();
        if ( ($wikiUserId != null) && ($wikiUserId > 0) && ($secretId > -1)) {
          $dbr  = wfGetDB( DB_SLAVE);
          $rows = $dbr->select('pws_rights',
                               array('user_id'),
                               "secret_id = '$secretId' AND group_id = -1 AND right_type = 1",
                               '',
                               '');
          foreach( $rows as $row ) { 
            $allowedEditorIds .= $row->user_id.', ' ;
          }
          $allowedEditorIds .= $ownerId.')';
          $dbr  = wfGetDB( DB_SLAVE);
          $rows = $dbr->select('user',
                              array('user_id', 'user_name', 'user_real_name' ),
                              "user_id IN ".$allowedEditorIds,
                              '',
                              '');
          foreach( $rows as $row ) { 
            $row->isGroup     = 0;   
            $allowedEditors[] = $row;
          }
        
          // Search for Groups
          $rows = $dbr->select('pws_rights',
                               array('group_id'),
                               "secret_id = '$secretId' AND user_id = -1 AND right_type = 1",
                               '',
                               '');
          foreach( $rows as $row ) { 
            $allowedGroupIds .= $row->group_id.', ';
          }
          $allowedGroupIds .= ' 0)';
          $rows = $dbr->select('pws_groups',
                               array('id', 'name'),
                               "id IN ".$allowedGroupIds,
                               '',
                               '');          
          foreach( $rows as $row ) {
            $row->isGroup     = 1; 
            $allowedEditors[] = $row;            
          }          
        }        
        $this->getResult()->addValue(  
          null, 
          $this->getModuleName(),
          array ( 
            'allowedEditors'  => $allowedEditors
          ) 
        );           
        break; 


      case 'getGroups' :
        deb('getGroups method called');
        $allGroups  = array();
        $wikiUserId = $wgUser->getId();
        $dbr        = wfGetDB( DB_SLAVE);        
        $rows       = $dbr->select('pws_groups',
                               array('id', 'name', 'owner'),
                               '',
                               '',
                               '');
        foreach( $rows as $row ) { 
          if ($this->canEditGroup($row->id, $wikiUserId)){
            $row->editable = 1;
          } else {
            $row->editable = 0;
          }
          $allGroups[] = $row;          
        }
        $this->getResult()->addValue(  
          null, 
          $this->getModuleName(),
          array ( 
            'allGroups' => $allGroups
          ) 
        );       
        break;
      
      case 'createGroup' :
        deb('createGroup called');
        $groupName  = $this->getMain()->getVal( 'groupName' );
        $ownerId    = $wgUser->getId();
        if (($groupName != null) && (strlen($groupName) >= 2) && ($ownerId > 0)) {
          $dbw = wfGetDB(DB_MASTER);
          $dbw->insert( 'pws_groups', array( 'name' => $groupName, 'owner' => $ownerId));
        };
        break; 

      
      case 'removeGroup' :
        deb('removeGroup called');
        $groupId    = $this->getMain()->getVal('groupId');
        $wikiUserId = $wgUser->getId();
        if (($groupId > 0) && ($wikiUserId > 0) && ($this->canEditGroup($groupId, $wikiUserId) == true)){
          $dbr = wfGetDB( DB_MASTER);
          // Delete Group
          $dbr->delete('pws_groups',"id = '$groupId'",''); 
          // Delete User Group Relation
          $dbr->delete('pws_user_of_group',"group_id = '$groupId'",'');   
          // Delete Access Rights for this group
          $dbr->delete('pws_rights',"group_id = '$groupId'",'');   
        }
        break;  


      case 'getGroupMembers' :
        deb('getGroupMembers called');
        $groupId      = $this->getMain()->getVal( 'groupId' );        
        $groupMembers = array();        
        if (($groupId != null) && ($groupId > 0)){
          $memberIds = '(';
          $dbr       = wfGetDB( DB_SLAVE );
          $rows      = $dbr->select( 'pws_user_of_group',
                                     array('user_id'),
                                     "group_id = '$groupId'",
                                     '',
                                     ''
          );          
          foreach( $rows as $row ) { 
             $memberIds .= $row->user_id.', ' ;            
          }
          $memberIds .= $this->getOwnerIdOfGroup($groupId).')';       
          $rows = $dbr->select('user',
                              array('user_id', 'user_name', 'user_real_name' ),
                              "user_id IN ".$memberIds,
                              '',
                              '');
          foreach( $rows as $row ) { 
            $groupMembers[] = $row;
          }                    
        }  
        $this->getResult()->addValue(  
          null, 
          $this->getModuleName(),
          array ( 
            'groupMembers' => $groupMembers
          ) 
        );        
        break;


      
      case 'getGroupEditors' :
        // this Action is allowed to EveryOne
        deb('getGroupEditors');  
        $groupId      = $this->getMain()->getVal( 'groupId' );        
        $groupEditors = array();
        
        if (($groupId != null) && ($groupId > 0)){
          $editorIds = '(';
          $dbr       = wfGetDB( DB_SLAVE );
          $rows      = $dbr->select( 'pws_rights',
                                     array('user_id'),
                                     "group_id = '$groupId' AND secret_id = -1",
                                     '',
                                     ''
          );          
          foreach( $rows as $row ) { 
             $editorIds .= $row->user_id.', ' ;            
          }
          $editorIds .= $this->getOwnerIdOfGroup($groupId).')';       
          $rows = $dbr->select('user',
                              array('user_id', 'user_name', 'user_real_name' ),
                              "user_id IN ".$editorIds,
                              '',
                              '');
          foreach( $rows as $row ) { 
            $groupEditors[] = $row;
          }          
          deb($editorIds);
        }  
        $this->getResult()->addValue(  
          null, 
          $this->getModuleName(),
          array ( 
            'groupEditors' => $groupEditors
          ) 
        );        
        break;


      case 'setGroupEditor' :
        deb('setGroupEditor called');
        $groupId    = $this->getMain()->getVal('groupId');
        $editorId   = $this->getMain()->getVal('editorId');
        $wikiUserId = $wgUser->getId();
        if ( $this->canEditGroup($groupId, $wikiUserId) == true ){
          if ( $groupId > 0 ) {
            $dbw = wfGetDB(DB_MASTER);
            $dbw->insert( 'pws_rights', array( 'secret_id' => -1, 'user_id' => $editorId , 'group_id' => $groupId, 'right_type' => 1 ) );
          }
        }
        break;


      case 'setGroupMember' :
        deb('setGroupMember called');
        $groupId    = $this->getMain()->getVal('groupId');
        $memberId   = $this->getMain()->getVal('memberId');
        $wikiUserId = $wgUser->getId();
        if ( $this->canEditGroup($groupId, $wikiUserId) == true ){
          if ( $groupId > 0 ) {
            $dbw = wfGetDB(DB_MASTER);
            $dbw->insert( 'pws_user_of_group', array( 'group_id' => $groupId, 'user_id' => $memberId) );
          }
        }
        break;


      case 'removeGroupEditor':
        deb('removeGroupEditor called');  
        $groupId    = $this->getMain()->getVal('groupId');
        $editorId   = $this->getMain()->getVal('editorId');
        $wikiUserId = $wgUser->getId();
        if ( $this->canEditGroup($groupId, $wikiUserId) == true ){
          if ( $groupId > 0 ) {
            $dbw = wfGetDB(DB_MASTER);
            $dbw->delete( 'pws_rights', array( 'secret_id' => -1, 'user_id' => $editorId , 'group_id' => $groupId, 'right_type' => 1 ) );
          }
        }
        break;


      case 'removeGroupMember':
        deb('removeGroupMember called');  
        $groupId    = $this->getMain()->getVal('groupId');
        $memberId   = $this->getMain()->getVal('memberId');
        $wikiUserId = $wgUser->getId();
        if ( $this->canEditGroup($groupId, $wikiUserId) == true ){
          if ( $groupId > 0 ) {
            $dbw = wfGetDB(DB_MASTER);
            $dbw->delete( 'pws_user_of_group', array( 'group_id' => $groupId, 'user_id' => $memberId) );
          }
        }
        break;


      default : 
        deb('default method called');
        $this->getResult()->addValue( 
          null, 
          $this->getModuleName(),
          array ( 'error' => $method.' method is not supported!')
        );
        break;  
    }

    return true;
  }

  // Description for this API Mechanism
  public function getDescription() {
    return 'For Ajax Communication with the ServerSide';
  }


  // This defines the allowed Parameters
  // Face parameter.
  public function getAllowedParams() {
    deb('PassWordSafeApi getAllowedParams');
    // The parent has no allowed params here! 
    // return array_merge( parent::getAllowedParams(), array(
    return array(
      'method' => array (
        ApiBase::PARAM_TYPE => 'string',
        ApiBase::PARAM_REQUIRED => true
      ),
    );
  }


  // Describe the parameter
  public function getParamDescription() {
    return array_merge( parent::getParamDescription(), array(
      'method' => 'To Define which remote procedure call do execute!'
    ));
  }


  // private Methods in this Class
  private function secretExists($secretKey){
    if ($this->getIdOfSecret($secretKey) > -1){
      return true;
    } else {
      return false;
    }
  }


  private function getOwnerIdOfSecret($secretId){
    $ownerId = -1;
    if ($secretId > -1){
      $dbr = wfGetDB( DB_SLAVE );          
      $row = $dbr->selectRow('pws_secrets',
                             array('owner'),
                             "id = '$secretId'",
                             '',
                             '');
      if ($row != false){
        $ownerId = $row->owner;
      }
    }             
    return $ownerId;    
  }


  private function getOwnerIdOfGroup($groupId){
    $ownerId = -1;
    if ($groupId > -1){
      $dbr = wfGetDB( DB_SLAVE );          
      $row = $dbr->selectRow('pws_groups',
                             array('owner'),
                             "id = '$groupId'",
                             '',
                             '');
      if ($row != false){
        $ownerId = $row->owner;
      }
    }             
    return $ownerId;    
  }  


  private function getIdOfSecret($secretKey){
    $secretId = -1;
    if ($secretKey != null){
      $dbr = wfGetDB( DB_SLAVE );          
      $row = $dbr->selectRow('pws_secrets',
                             array('id'),
                             "pw_key = '$secretKey'",
                             '',
                             '');
      if ($row != false){
        $secretId = $row->id;
      }
    }             
    return $secretId;
  }


  private function isEditableByUser($secretId, $wikiUserId){
    $editable = false;
    // Are we the Owner of this secret?
    $ownerId = $this->getOwnerIdOfSecret($secretId); 
    if (($ownerId > -1) && ($ownerId == $wikiUserId)){
      $editable = true;
    };
    // checking single rights -> if one exits we can read
    if (($editable == false) && ($secretId > -1) && ($wikiUserId != null) && ($wikiUserId > 0)){
      $dbr = wfGetDB( DB_SLAVE );          
      $row = $dbr->selectRow('pws_rights',
                          array('id', 'right_type'),
                          "user_id = '$wikiUserId' AND secret_id ='$secretId' AND group_id = -1 AND right_type = 1",
                          '',
                          '');          
      if ($row != false){ $editable = true; }    
    }
    // checking groups
    // is there a group for this user and this secret
    if (($editable == false) && ($secretId > -1) && ($wikiUserId != null) && ($wikiUserId > 0)){
      // we get all group_ids with read Access to this secret
      $groupIds = '(';
      $rows = $dbr->select('pws_rights',
                          array('id', 'group_id', 'right_type'),
                          "user_id = -1 AND secret_id ='$secretId' AND right_type = 1",
                          '',
                          '');   
      foreach( $rows as $row ) { 
        $groupIds .= $row->group_id.', ';
      }                        
      $groupIds .= '0)';  // Trick because there is now group with index 0
      // finding a row with group_id in groupIds and user_id = wikiUserId enables read access
      $row = $dbr->selectRow(
        'pws_user_of_group',
        array('group_id', 'user_id'),        
        'user_id = ' . $wikiUserId . ' AND group_id IN '. $groupIds,
        '',
        ''
      );
      if ($row != false ){ $editable = true; }                                 
      
    }
    return $editable; 
  }

  
  // here we have to check if the user is the owner of the secret or is in any right bound to this secret
  private function isViewableByUser($secretId, $wikiUserId){
    $viewable = false;
    // checking owner 
    $ownerId  = $this->getOwnerIdOfSecret($secretId); 
    if (($ownerId > -1) && ($ownerId == $wikiUserId)){
      $viewable = true;
    };
    // checking single rights -> if one exits we can read
    if (($viewable == false) && ($secretId > -1) && ($wikiUserId != null) && ($wikiUserId > 0)){
      $dbr = wfGetDB( DB_SLAVE );          
      $row = $dbr->selectRow(
        'pws_rights',
        array('id', 'right_type'),
        'user_id = '.$wikiUserId.' AND secret_id = '.$secretId.' AND group_id = -1 AND right_type = 0',
        '',
        ''
      );          
      if ($row != false){ $viewable = true; }    
    }
    // checking groups
    // is there a group for this user and this secret
    if (($viewable == false) && ($secretId > -1) && ($wikiUserId != null) && ($wikiUserId > 0)){
      // we get all group_ids with read Access to this secret
      $groupIds = '(';
      $rows = $dbr->select(
        'pws_rights',
        array('id', 'group_id', 'right_type'),
        "user_id = -1 AND secret_id ='$secretId' AND right_type = 0",
        '',
        ''
      );   
      foreach( $rows as $row ) { 
        $groupIds .= $row->group_id.', ';
      }                        
      $groupIds .= '0)';  // Trick because there is now group with index 0
      // finding a row with group_id in groupIds and user_id = wikiUserId enables read access      
      $row = $dbr->selectRow(
        'pws_user_of_group',
        array('group_id', 'user_id'),
        'user_id = ' . $wikiUserId . ' AND group_id IN '. $groupIds,        
        '',
        ''
      );
      if ($row != false ){ $viewable = true; }                                 
      
    }
    return $viewable;
  }


  // ToDo: Has to be implemented
  private function canEditGroup($groupId, $userId){
    deb('canEditGroup called');
    $result = false;
    if (($groupId > 0) && ($userId > 0)){
      $dbr    = wfGetDB(DB_SLAVE);
      $row    = $dbr->selectRow(
        'pws_groups',
        array('id', 'name', 'owner'),
        "id = '$groupId' AND owner = '$userId'",
        '',
        ''
      );
      if ($row != false){ $result = true; }
      if ($result == false){
        $row    = $dbr->selectRow(
          'pws_rights',
          array('id'),
          "group_id = '$groupId' AND user_id = '$userId' AND right_type = 1",
          '',
          ''
          );
        if ($row != false){ $result = true; }
      } 
    }  

    return $result;
  }


  private function Bool2Num($aBool){
    if ($aBool == true){
      return 1;
    } else {
      return 0;
    }
  }
  
}