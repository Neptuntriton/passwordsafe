

<?php

  // Build with the Help of this
  // https://www.mediawiki.org/wiki/Manual:Special_pages

  class SpecialPassWordSafe extends SpecialPage {
    function __construct() {
      parent::__construct( 'PassWordSafe' );
    }

    
    function execute( $par ) {

      $request = $this->getRequest();
      $output  = $this->getOutput();
      $this->setHeaders();

      # Get request data from, e.g.
      $param = $request->getText( 'param' );

      $output->addWikiText( '=== See, Add or Edit Groups for PassWordSafe ===' );
      $output->addWikiText('This groups are only for the PWS Extension. Groups allow you to gather users in groups to give them read or edit access to certain secrets managed by the PWS Extension. Please note the following rules:');
      $output->addWikiText('* Groups can be created by every logged in User. The creator is called owner of the group.');
      $output->addWikiText('* The owner can manage any group he is owner off.');
      $output->addWikiText('* Every user logged in can see all groups and its members.');
      $output->addWikiText('=== Create a new group ===');      
      $output->addHTML( "<div class='pws_group_creating'> </div>" );
      $output->addWikiText('=== List of known groups ===');      
      $output->addHTML( "<div class='pws_group_list'> </div>" );
    }
  }